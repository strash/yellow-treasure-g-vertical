extends Control


const IMAGES: Array = [
	"wallpapper_1",
	"wallpapper_2",
	"wallpapper_3",
	"wallpapper_4",
	"wallpapper_5",
	"wallpapper_6",
]


# BUILTINS - - - - - - - - -


func _ready() -> void:
	show_wallpapper(false)


# METHODS - - - - - - - - -


func show_wallpapper(state: bool) -> void:
	var _t: int
	if state:
		($Wallpaper as Control).call("set_image")
		($Wallpaper as Control).show()
		_t = ($Tween as Tween).interpolate_property($Wallpaper as Control, "modulate:a", 0.0, 1.0, 0.3)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
	else:
		_t = ($Tween as Tween).interpolate_property($Wallpaper as Control, "modulate:a", 1.0, 0.0, 0.3)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(0.3), "timeout")
		($Wallpaper as Control).hide()


# SIGNALS - - - - - - - - -


func _on_Btn1_pressed() -> void:
	($Wallpaper as Control).set("IMAGE_NAME", IMAGES[0])
	show_wallpapper(true)


func _on_Btn2_pressed() -> void:
	($Wallpaper as Control).set("IMAGE_NAME", IMAGES[1])
	show_wallpapper(true)


func _on_Btn3_pressed() -> void:
	($Wallpaper as Control).set("IMAGE_NAME", IMAGES[2])
	show_wallpapper(true)


func _on_Btn4_pressed() -> void:
	($Wallpaper as Control).set("IMAGE_NAME", IMAGES[3])
	show_wallpapper(true)


func _on_Btn5_pressed() -> void:
	($Wallpaper as Control).set("IMAGE_NAME", IMAGES[4])
	show_wallpapper(true)


func _on_Btn6_pressed() -> void:
	($Wallpaper as Control).set("IMAGE_NAME", IMAGES[5])
	show_wallpapper(true)
