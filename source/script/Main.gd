extends Node


# BUILTINS - - - - - - - - -


func _ready() -> void:
	randomize()

	var _level1_pressed = ($Levels as Control).connect("level1_pressed", self, "_on_MyLevels_level1_pressed")
	var _level2_pressed = ($Levels as Control).connect("level2_pressed", self, "_on_MyLevels_level2_pressed")
#	var _level3_pressed = ($Levels as Control).connect("level3_pressed", self, "_on_MyLevels_level3_pressed")

	var _menu_item_pressed = ($GUI as Control).connect("menu_item_pressed", self, "_on_MyGUI_menu_item_pressed")
	var _btn_back_pressed = ($GUI as Control).connect("btn_back_pressed", self, "_on_MyGUI_btn_back_pressed")
	var _btn_spin_pressed = ($GUI as Control).connect("btn_spin_pressed", self, "_on_MyGUI_btn_spin_pressed")
	var _btn_autospin_pressed = ($GUI as Control).connect("btn_autospin_pressed", self, "_on_MyGUI_btn_autospin_pressed")

	var _score_increased = ($Game as Control).connect("score_increased", self, "_on_MyGame_score_increased")
	var _spinnig_changed = ($Game as Control).connect("spinnig_changed", self, "_on_MyGame_spinnig_changed")
	var _auto_spinnig_changed = ($Game as Control).connect("auto_spinnig_changed", self, "_on_MyGame_auto_spinnig_changed")
	var _autospin_count_changed = ($Game as Control).connect("autospin_count_changed", self, "_on_MyGame_autospin_count_changed")

	set_slot_view(1)
	($GUI as Control).call("show_hide_controls", false)


# METHODS - - - - - - - - -


func set_view(view: int) -> void:
	var _t: int
	match view:
		0:
			($Wallpappers as Control).show()
			_t = ($Tween as Tween).interpolate_property($Wallpappers as Control, "modulate:a", 0.0, 1.0, 0.3)
			if not ($Tween as Tween).is_active():
				_t = ($Tween as Tween).start()
			set_slot_view(1)
			($Wallpappers as Control).call("show_wallpapper", false)
		1:
			_t = ($Tween as Tween).interpolate_property($Wallpappers as Control, "modulate:a", 1.0, 0.0, 0.3)
			if not ($Tween as Tween).is_active():
				_t = ($Tween as Tween).start()
			set_slot_view(1)
			($Wallpappers as Control).call("show_wallpapper", false)
			yield(get_tree().create_timer(0.3), "timeout")
			($Wallpappers as Control).hide()


func set_slot_view(view: int) -> void:
	var _t: int
	match view:
		1:
			($Levels as Control).show()
			_t = ($Tween as Tween).interpolate_property($Levels, "modulate:a", 0.0, 1.0, 0.3)
			_t = ($Tween as Tween).interpolate_property($Game, "modulate:a", 1.0, 0.0, 0.3)
			if not ($Tween as Tween).is_active():
				_t = ($Tween as Tween).start()
			yield(get_tree().create_timer(0.3), "timeout")
			($Game as Control).hide()
		2:
			($Game as Control).show()
			_t = ($Tween as Tween).interpolate_property($Levels, "modulate:a", 1.0, 0.0, 0.3)
			_t = ($Tween as Tween).interpolate_property($Game, "modulate:a", 0.0, 1.0, 0.3)
			if not ($Tween as Tween).is_active():
				_t = ($Tween as Tween).start()
			yield(get_tree().create_timer(0.3), "timeout")
			($Levels as Control).hide()


# SIGNALS - - - - - - - - -


func _on_MyGUI_menu_item_pressed(menu_item: int) -> void:
	set_view(menu_item)


func _on_MyLevels_level1_pressed() -> void:
	set_slot_view(2)
	($Game as Control).call("prepare_level", 1)
	($GUI as Control).call("show_hide_controls", true)


func _on_MyLevels_level2_pressed() -> void:
	set_slot_view(2)
	($Game as Control).call("prepare_level", 2)
	($GUI as Control).call("show_hide_controls", true)


#func _on_MyLevels_level3_pressed() -> void:
#	set_slot_view(2)
#	($Game as Control).call("prepare_level", 3)
#	($GUI as Control).call("show_hide_controls", true)


func _on_MyGUI_btn_back_pressed() -> void:
	set_slot_view(1)
	($Game as Control).call("clear_level")
	($GUI as Control).call("show_hide_controls", false)


func _on_MyGUI_btn_spin_pressed() -> void:
	($Game as Control).call("spin")


func _on_MyGUI_btn_autospin_pressed() -> void:
	($Game as Control).call("auto_spin")


func _on_MyGame_score_increased() -> void:
	($GUI as Control).call("set_score")


func _on_MyGame_spinnig_changed(status: bool) -> void:
	($GUI as Control).set("spinning", status)


func _on_MyGame_auto_spinnig_changed(status: bool) -> void:
	($GUI as Control).set("auto_spinning", status)
	if not status:
		($GUI as Control).call("show_hide_autospin_progress", false)


func _on_MyGame_autospin_count_changed(auto_spin_count: int, auto_spin_max: int) -> void:
	($GUI as Control).call("change_autospin_count", auto_spin_count, auto_spin_max)
