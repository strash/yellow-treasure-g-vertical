extends Control


var IMAGE_NAME: String = ""


# BUILTINS - - - - - - - - -


#func _ready() -> void:
#	pass


# METHODS - - - - - - - - -


func set_image() -> void:
	($Image as TextureRect).texture = load("res://source/assets/image/bg/wallpapper/%s.png" % IMAGE_NAME)


# SIGNALS - - - - - - - - -

func _on_BtnDownload_pressed() -> void:
	var js = """
	const a = document.createElement('a');
	a.href = "wallpapper/%s.png";
	a.setAttribute("download", "%s.png")
	a.style.display = 'none';
	document.body.appendChild(a);
	a.click();
	a.remove();
	""" % [IMAGE_NAME, IMAGE_NAME]
	JavaScript.eval(js)
